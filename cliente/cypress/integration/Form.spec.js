/* eslint-disable */
///<reference types="cypress" />
describe('<Form />', () => {
    it('<Login /> - Frond Verify Home', () => {
        cy.visit('/');
        // Search Text Not Recommend
        cy.contains('h1', 'Iniciar Sesión');
        // Search Text Success
        cy.get('[data-cy=title]')
            .invoke('text')
            .should('equal', 'Iniciar Sesión');

        //Check Form  Login Exist
        cy.get("[data-cy=form-login]").should('exist');

        //Check Form  Login Inputs
        cy.get("[data-cy=input-email]").should('exist');
        cy.get("[data-cy=input-password]").should('exist');

        //Check Form  Login Input Submit
        cy.get("[data-cy=submit-login]").should('exist')
                                        .should('have.value', 'Iniciar Sesión')
                                        .should('have.class', 'btn-primario')
                                        .should('have.class', 'btn');
        //Check Form  Login Input New Account
        cy.get("[data-cy=new-account]").should('exist')
            .should('have.attr', 'href')
            .should('eq', '/nueva-cuenta')

        //Check Validate Link Enlace
        cy.get("[data-cy=new-account]").should('exist')
                                        .should('have.prop', 'tagName');

        cy.visit('/nueva-cuenta');

    });

    it('<NuevaCuenta /> - Frond Verify New Account', () => {
        //Validate Title
        cy.get('[data-cy=title]').should('exist')
            .invoke('text')
            .should('equal','Obtener una cuenta')
        cy.get('[data-cy=form-new-account]').should('exist')
        cy.get('[data-cy=input-name]').should('exist')
        cy.get('[data-cy=input-password]').should('exist')
            .should('have.prop', 'type')
            .should('equal', 'password');

        cy.get('[data-cy=input-password-repeat]')
            .should('exist')


        cy.get('[data-cy=input-submit]')
            .should('exist')
            .should('have.class', 'btn-primario')
            .should('have.value', 'Registrarme')
            .should('not.have.value', 'Nueva Cuenta');

        cy.get('[data-cy=new-session]')
            .should('exist')
            .should('have.attr', 'href')
            .should('eq', '/')

        cy.visit('/')

    });

})
