/* eslint-disable */
///<reference types="cypress" />
describe('<Login',()=>{
    it('<Login> Review Auth Validation Alerts', () => {
        cy.visit('/');

        //Add Action Form Login
        cy.get('[data-cy=submit-login]').click()

        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-login]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Todos los campos son obligatorios')

        cy.get('[data-cy=alert-login]')
            .should('have.class', 'alerta-error');

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-email]').type('vlzdavid12@outlook.com')
        cy.get('[data-cy=input-password]').type('1234')

        //Add Action Form Login
        cy.get('[data-cy=submit-login]').click()

        //Alert Validate Password
        cy.get('[data-cy=alert-login]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Password Incorrecto')

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-email]').clear().type('vlzdavid14@outlook.com')
        cy.get('[data-cy=input-password]').clear().type('123456')

        //Add Action Form Login
        cy.get('[data-cy=submit-login]').click()

        //Alert Validate Password
        cy.get('[data-cy=alert-login]')
            .should('exist')
            .invoke('text')
            .should('equal', 'El usuario no existe')


        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-email]').clear().type('vlzdavid12@outlook.com')
        cy.get('[data-cy=input-password]').clear().type('123456')

        //Add Action Form Login
        cy.get('[data-cy=submit-login]').click()

        //Action Close Session
        cy.get('[data-cy=close-session]').click();

        cy.visit('/');

    })
})
