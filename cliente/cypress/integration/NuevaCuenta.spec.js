/* eslint-disable */
///<reference types="cypress" />
describe('<NuevaCuenta>', ()=>{
    it('<NuevaCuenta /> - Frond Verify New Account Events', () => {
        cy.visit('/nueva-cuenta');

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        cy.get('[data-cy=alert-validate]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Todos los campos son obligatorios')

        cy.get('[data-cy=alert-validate]')
            .should('have.class', 'alerta-error')

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-name]').type('David')
        cy.get('[data-cy=input-email]').type('vlzdavid12@outlook.com')
        cy.get('[data-cy=input-password]').type('123')
        cy.get('[data-cy=input-password-repeat]').type('123')

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-validate]')
            .should('exist')
            .invoke('text')
            .should('equal', 'El password debe ser de al menos 6 caracteres')

        cy.get('[data-cy=alert-validate]')
            .should('have.class', 'alerta-error')

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-password]').clear().type('123456')
        cy.get('[data-cy=input-password-repeat]').clear().type('12456')

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-validate]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Los passwords no son iguales')


        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-email]')
            .clear()
            .type('vlzdavid12outlook.com')
        cy.get('[data-cy=input-password]')
            .clear()
            .type('123456')
        cy.get('[data-cy=input-password-repeat]')
            .clear()
            .type('123456')

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-name]').clear()
        cy.get('[data-cy=input-email]').clear()

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-validate]')
            .should('exist')
            .invoke('text')
            .should('equal', 'Todos los campos son obligatorios')

        //Insert Inputs Data Validate Form
        cy.get('[data-cy=input-name]').type('David')
        cy.get('[data-cy=input-email]').type('vlzdavid12@outlook.com')

        //Action Click Form
        cy.get('[data-cy=input-submit]').click()

        cy.get('[data-cy=select-draft]').should('exist')
            .invoke('text')
            .should('eq', 'Selecciona un proyecto')

        //Action Close Session
        cy.get('[data-cy=close-session]').click();
    });
        it('<NuevaCuenta>- Review Users Duplicate', ()=>{
            cy.visit('/nueva-cuenta')

            //Insert Inputs Data Validate Form
            cy.get('[data-cy=input-name]').type('David')
            cy.get('[data-cy=input-email]').type('vlzdavid12@outlook.com')
            cy.get('[data-cy=input-password]').type('123456')
            cy.get('[data-cy=input-password-repeat]').type('123456')

            //Action Click Form
            cy.get('[data-cy=input-submit]').click()

            //Alert Validate Password Min Six Characters
            cy.get('[data-cy=alert-validate]')
                .should('exist')
                .invoke('text')
                .should('equal', 'El usuario ya existe')

        });

});
