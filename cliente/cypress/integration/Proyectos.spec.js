/* eslint-disable */
///<reference types="cypress" />
describe('Admin', ()=>{
    it('<Login /> - Frond Auth Login', () => {
        cy.visit('/');
        cy.get('[data-cy=input-email]').type('vlzdavid12@outlook.com')
        cy.get('[data-cy=input-password]').type('123456')
        cy.get('[data-cy=submit-login]').click()
    });

    it('<Proyectos> - Validate Proyect', ()=>{
            //Validation
            cy.get('[data-cy=new-draft]').click()
            cy.get('[data-cy=submit-new-draft]').click()


        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-draft]')
            .should('exist')
            .invoke('text')
            .should('equal', 'El nombre del Proyecto es obligatorio')

        cy.get('[data-cy=alert-draft]')
            .should('have.class', 'mensaje error')
    })

    it('<Proyectos /> - Creación de Proyectos', () =>{
            cy.get('[data-cy=input-new-draft]').type('Tienda Virtual');
            cy.get('[data-cy=submit-new-draft]').click();

            //Select proyect
            cy.get('[data-cy=list-projects] li:nth-child(1) button').click();
    })

    it('<Tarea> - Validate and Create', ()=>{
        cy.get('[data-cy=submit-new-track]').click();

        //Alert Validate Password Min Six Characters
        cy.get('[data-cy=alert-track-require]')
            .should('exist')
            .invoke('text')
            .should('equal', 'El nombre de la tarea es obligatorio');

        cy.get('[data-cy=input-new-track]').type('Definir Nuevo Diseño');
        cy.get('[data-cy=submit-new-track]').click();

    })

    it('<Tareas> - Complete, Descomplete, Edit, Delete',()=>{

        //Select first track
        cy.get('[data-cy=track]:nth-child(1) [data-cy=working-incomplete]').click();
        cy.get('[data-cy=track]:nth-child(1) [data-cy=working-complete]').should('have.class', 'completo');

        //Action Edit new input text
        cy.get('[data-cy=track]:nth-child(1) [data-cy=edit-new-track]').click();
        cy.get('[data-cy=input-new-track]').clear().type('Modificando Para Definir Nuevo Diseño');

        //Action Button Edit Project Title
        cy.get('[data-cy=submit-new-track]').click();

        //Wait Event Time Moment
        cy.wait(5000)

        //Delete Track List
        cy.get('[data-cy=track]:nth-child(1) [data-cy=delete-new-track]').click();

        //Wait Event Time Moment
        cy.wait(5000)

        //Delete Track List
        cy.get('[data-cy=delete-project]').click();
    })



})
