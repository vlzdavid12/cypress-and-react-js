import React from 'react';

const Alert = ({alert}) => {
    if(!alert.categoria) return null;

    return (<div data-cy="alert-login" className={`alerta ${alert.categoria}`}>{alert.msg}</div>);

}
export default Alert
